<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng ký</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>    
    <style type="text/css">
        *:focus {
            outline: none;
        }

        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 80vh;
        }

        .validation {
            color:#FF3333;
        }

        .wrapper {
            width: 25%;
            padding: 40px;
            border: 2px solid #3f6db9;
        }

        .field {
            margin-bottom: 25px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .field__label {
            color: #eeeeee;
            background-color: #63a64e;
            padding: 12px 10px;
            width: 30%;
            border: 2px solid #3f6db9;
        }
        .field__input {
            font-size: 14px;
            font-weight: 400;
            padding: 12px;
            border-radius: 0;
            border: 1px solid #3f6db9;
            width: 55%;
        }

        .gender {
            display: flex;
            width: 55%;
            gap: 20px;
        }

        select {
            width: 55%;
            height: 43px;
            border: 1px solid #3f6db9;
        }

        .button {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .btn-submit {
            font-size: 16px;
            color: #eeeeee;
            background-color: #63a64e;
            padding: 12px 32px;
            margin-top: 15px;
            border-radius: 10px;
        }

    </style>
</head>
<body>

    <?php
        $genders = array("Nữ", "Nam");
        $faculties = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");

        // Create a directory
        if (!file_exists('upload')) {
            mkdir('upload', 0777, true);
        }

        // Delete directory
        // array_map('unlink', glob("upload/*.*"));
        // rmdir("upload");

        // Get String of date-time
        function getStringOfDate() {
            $date   = new DateTime(); //this returns the current date time
            $result = $date->format('Y-m-d-H-i-s');
            $krr    = explode('-', $result);
            $result = implode("", $krr);
            return $result;
        }
    ?>

    <div class="main">
        <div class="wrapper">

            <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $check = 0;

                    if(empty($_POST["fullname"])) {
                        echo '<span class="validation">Hãy nhập Họ tên</span> <br>';
                        $check++;
                    }

                    if(empty($_POST["gender"])) {
                        echo '<span class="validation">Hãy chọn Giới tính</span> <br>';
                        $check++;
                    }

                    if(empty($_POST["faculty"])) {
                        echo '<span class="validation">Hãy chọn Phân khoa</span> <br>';
                        $check++;
                    }

                    if(empty($_POST["birthday"])) {
                        echo '<span class="validation">Hãy nhập Ngày sinh</span> <br>';
                        $check++;

                    } else {
                        $date = explode("/",$_POST['birthday']);
                        if(!checkdate($date[1] ,$date[0] ,$date[2]))
                        {
                            echo '<span class="validation">Hãy nhập Ngày sinh đúng định dạng</span> <br>';
                            $check++;
                        }
                    }

                    if(!empty($_FILES["avatar"])) {
                        $acceptable = array(
                            'image/jpeg',
                            'image/png'
                        );

                        if(!in_array($_FILES['avatar']['type'], $acceptable) && (!empty($_FILES["avatar"]["type"]))) {
                            echo '<span class="validation">Hãy tải Hình ảnh lên đúng định dạng JPEG hoặc PNG</span> <br>';
                            $check++;
                        }
                    }

                    if($check == 0) {
                        session_start();
                        $_SESSION = $_POST; // Save information to Session

                        $filename = $_FILES["avatar"]["tmp_name"]; // Get file

                        $originalName = pathinfo($_FILES["avatar"]["name"], PATHINFO_FILENAME); // Get original file name
                        $extension = pathinfo($_FILES["avatar"]["name"], PATHINFO_EXTENSION); // Get extension name of file

                        $destination = "upload/" . $originalName . "_" . getStringOfDate() . "." . $extension; // Path to save image
                        move_uploaded_file($filename, $destination); // Save uploaded picture in your directory

                        $_SESSION['avatarUrl'] = $destination;
                    }

                    if(isset($_SESSION)) {
                        header("Location:confirm.php");
                    }

                }
            ?>
            <br>

            <form action="" method="post" enctype="multipart/form-data">
                <div class="field">
                    <label for="fullname" class="field__label">Họ và tên *</label>
                    <input type="text" name="fullname" class="field__input"/>
                </div>

                <div class="field">
                    <label class="field__label">Giới tính *</label>
                    <div class="gender">
                        <?php
                            for($i = 0; $i < count($genders); $i++) {
                                echo '<div>';
                                echo '<input type="radio" id="' . $genders[$i] . '" name="gender" value="' . $i+1 . '">';
                                echo '<label style="margin-left: 12px; user-select: none; cursor: pointer" for="' . $genders[$i] . '">' . $genders[$i] . '</label><br>';
                                echo '</div>';

                            }
                        ?>
                    </div>
                    
                </div>

                <div class="field">
                    <label for="faculty" class="field__label">Phân khoa *</label>
                    <select name="faculty">
                        <option value="0" selected disabled></option>
                        <?php
                            foreach($faculties as $x => $x_value) {
                                echo '<option value="' . $x . '">' . $x_value . '</option>';
                            }
                        ?>
                    </select>
                </div>

                <div class="field">
                    <label for="birthday" class="field__label">Ngày sinh *</label>
                    <input name="birthday" class="date form-control field__input" type="text" style="padding: 22px 12px;">
                </div>

                <div class="field">
                    <label for="address" class="field__label">Địa chỉ</label>
                    <input type="text" name="address" class="field__input"/>
                </div>

                <div class="field">
                    <label for="avatar" class="field__label">Hình ảnh</label>
                    <input type="file" name="avatar" id="avatar" class="field__input"/>
                </div>

                <div class="button">
                    <button type="submit" class="btn-submit">Đăng ký</button>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">  
        $('.date').datepicker({
           format: 'dd/mm/yyyy'
         });
    </script>
</body>

</html>