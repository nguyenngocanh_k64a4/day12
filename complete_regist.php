<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Xác nhận thông tin Sinh viên</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        *:focus {
            outline: none;
        }

        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 80vh;
        }

        .wrapper {
            width: 30%;
            padding: 40px;
            border: 2px solid #3f6db9;
            text-align: center;
        }

    </style>
</head>
<body>
<div class="main">
    <div class="wrapper">
        <h3>Bạn đã đăng ký thành công sinh viên</h3>
        <br>
        <a href="list-students.php">Quay lại danh sách sinh viên</a>
    </div>
</div>
</body>

</html>