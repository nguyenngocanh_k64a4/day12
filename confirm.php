<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Xác nhận thông tin Sinh viên</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        *:focus {
            outline: none;
        }

        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 80vh;
        }

        .wrapper {
            width: 25%;
            padding: 40px;
            border: 2px solid #3f6db9;


        }

        .field {
            margin-bottom: 25px;
            display: flex;
            align-items: center;
            gap: 50px;
        }

        .field__label {
            color: #eeeeee;
            background-color: #63a64e;
            padding: 12px 10px;
            width: 30%;
            border: 2px solid #3f6db9;
        }

        .field > div {
            width: 58%;
        }

        .button {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .btn-submit {
            font-size: 16px;
            color: #eeeeee;
            background-color: #5fa14a;
            padding: 12px 32px;
            margin-top: 15px;
            border-radius: 10px;
        }

    </style>
</head>
<body>
    <div class="main">
        <div class="wrapper">
            <?php
                session_start();
//                print_r($_SESSION);
                include 'utilities/db_connection.php';

                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    // Create connection
                    $conn = OpenCon();
                    // echo "Connected Successfully";

                    // Prepare and bind
                    $stmt = $conn->prepare("INSERT INTO student (name, gender, faculty, birthday, address, avatar) VALUES (?, ?, ?, ?, ?, ?)");
                    $stmt->bind_param("sissss", $name, $gender, $faculty, $birthday, $address, $avatar);

                    // Set parameters
                    $name = $_SESSION["fullname"];
                    $gender = $_SESSION["gender"] - 1;
                    $faculty = $_SESSION["faculty"];
                    $birthday = str_replace('/', '-', $_SESSION["birthday"]);
                    $birthday = date('Y-m-d', strtotime($birthday));
                    $address = $_SESSION["address"];
                    $avatar = $_SESSION["avatarUrl"];
                    // Execute
                    $stmt->execute();

                    $stmt->close();
                    $conn->close();

                    header("Location:complete_regist.php");

                }
            ?>
            <form method="POST">
                <div class="field">
                    <label for="fullname" class="field__label">Họ và tên</label>
                    <div>
                        <?php
                            if($_SESSION["fullname"]) {
                                echo '<span>' . $_SESSION["fullname"] . '</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label class="field__label">Giới tính</label>
                    <div>
                        <?php
                            if($_SESSION["gender"] == 1) {
                                echo '<span>Nam</span>';
                            } else if($_SESSION["gender"] == 2) {
                                echo '<span>Nữ</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="faculties" class="field__label">Phân khoa</label>
                    <div>
                        <?php
                            if($_SESSION["faculty"] == 'MAT') {
                                echo '<span>Khoa học máy tính</span>';
                            } else if($_SESSION["faculty"] == 'KDL') {
                                echo '<span>Khoa học vật liệu</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="bithday" class="field__label">Ngày sinh</label>
                    <div>
                        <?php
                            if($_SESSION["birthday"]) {
                                echo '<span>' . $_SESSION["birthday"] . '</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="address" class="field__label">Địa chỉ</label>
                    <div>
                        <?php
                            if($_SESSION["address"]) {
                                echo '<span>' . $_SESSION["address"] . '</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="avatar" class="field__label">Hình ảnh</label>
                    <div>
                        <?php
                            if($_SESSION["avatarUrl"]) {
                                echo '<img src="'. $_SESSION["avatarUrl"] .'" alt="Avatar" width="150" height="100">';
                            }
                        ?>
                    </div>
                </div>

                <div class="button">
                    <button type="submit" class="btn-submit">Xác nhận</button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>